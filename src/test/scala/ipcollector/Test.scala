package ipcollector

import actors.Futures._

class Test extends org.scalatest.FunSuite {

  val ips = Seq(
    "1.1.1.1", "1.1.1.1", "4.4.4.4",
    "1.1.1.1", "2.2.2.2", "3.3.3.3",
    "6.6.6.6", "1.1.1.1", "5.5.5.5", "4.4.4.4",
    "1.1.1.1", "2.2.2.2", "2.2.2.2", "7.7.7.7",
    "1.1.1.1", "2.2.2.2", "5.5.5.5", "4.4.4.4")


  test("simple") {
    IPCollector.NTRESHOLD = 6
    IPCollector.NLIMIT = 5
    IPCollector.reset
    ips.foreach(ip => IPCollector.collect(ip))
    assert(IPCollector.getTop(2) === Seq("1.1.1.1", "2.2.2.2"))
  }

  test("performance") {
    IPCollector.NTRESHOLD = 10000
    IPCollector.NLIMIT = 1000
    IPCollector.reset
    println("starting performance test")
    Seq(1, 2, 3).map(id => future[Int] {
      for (i <- 1 to 20000)
        ips.map(e => { val j = i%10; if (e.charAt(0) == j+'0') i%200 + e.substring(1) else e }).foreach(ip => IPCollector.collect(ip))
      IPCollector.commit()
      id
    }).foreach(f => println(f()))
    assert(IPCollector.getTop(2) === Seq("1.1.1.1", "2.2.2.2"))
    println(IPCollector.getTop(50))
    println(IPCollector.topByIP)
  }
}
